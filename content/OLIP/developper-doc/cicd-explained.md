---
title: CI/CD explained
comments: false
pre: "<b>- </b>"
---

## Synopsis

Whenever commits are nerged to the `master` branch,
the CICD builds and publishes your Docker image.

{{% notice info %}}
All OLIP Apps now share a common CICD pipeline: <https://bibliosansfrontieres.gitlab.io/olip/cicd/>
{{% /notice %}}

## Core components

A basic, minimal OLIP deployment requires both `olip-api` and `olip-dashboard`.

<!--
The `olip-deploy` playbook deploys these images to a target server.

The `master` branch http://olip.bibliosansfrontieres.org/ |

As stated before, these repositories pipelines do not perform the deployment.

Their deploy job calls for GitLab API, passing their `environment` as a variable
to trigger `olip-deploy` 's CI.

`olip-deploy`, in turn, creates a new pipeline to perform the deployment
of all required images to the targeted environment.

Note that the environment variable passed via the API call is only used by
the `rules:` statements, and does not define by itself the target environment.

-->

For now the deployment to the Production server has to be started manually.
This is subject to change in the future.

## Applications

Applications are automatically built and pushed to the Docker Hub,
making them available to any OLIP instance.

The catalog is **not** updated by the CICD,
you have to do it manually.

## Development, staging, latest images

Feel free to make use of Docker tags in order to test your App.

In production, what really matters is the tag that is used in the
`descriptor.json` file the OLIP instance is linked to:

```shell
$ curl -sL http://olip.ideascube.org/amd64/descriptor.json \
    | jq '.applications | .[].containers | .[].image'
"registry.gitlab.com/bibliosansfrontieres/olip/apps/hugo-mediacenter/hugo-mediacenter:latest"
"registry.gitlab.com/bibliosansfrontieres/olip/apps/kolibri/kolibri:latest"
"registry.gitlab.com/bibliosansfrontieres/olip/apps/kiwix/kiwix:latest"
"registry.gitlab.com/bibliosansfrontieres/olip/apps/nextcloud/nextcloud:latest"
"registry.gitlab.com/bibliosansfrontieres/olip/apps/surfer/surfer:latest"
"registry.gitlab.com/bibliosansfrontieres/olip/apps/writefreely/writefreely:latest"
"registry.gitlab.com/bibliosansfrontieres/olip/apps/moodle/moodle:latest"
"registry.gitlab.com/bibliosansfrontieres/olip/apps/mariadb/mariadb:latest"
"registry.gitlab.com/bibliosansfrontieres/olip/apps/karibu/karibu:latest"
"registry.gitlab.com/bibliosansfrontieres/olip/apps/mariadb/mariadb:latest"
"offlineinternet/dweb-mirror:latest"
```
