---
title: Developper documentation
comments: false
weight: 6
pre: "<b>6. </b>"
---

This part is about the OLIP development (frontend, backend).

## The frontend

It is basically a client for olip-api.

## The backend

It's an API.

## Development process

Basically:

* setup your local development environment
* hack, commit, push and MR
* check for the review-app environments that everything runs correctly
* merge and profit!

## CICD

We are migrating to a common, shared CI/CD pipeline.
