---
title: What it is ?
comments: false
weight: 1
pre: "<b>1. </b>"
---
OLIP is an open source software developed by the offline Internet consortium to turn any device into an offline server.

It makes it easy to install WebApps and manage their content on your server.

OLIP provides a search engine for global search within each WebApp, and hosts an Authentication server based on OAuth.

Online demo instance: <http://olip.bibliosansfrontieres.org/>

![](../../image-20200114111001085.png)

## How it works

OLIP is a Python/VueJS platform that aims to automate WebApp & content installation through Docker engine. The system runs only on Linux based system. 
Hardware with wifi capability can turn their wifi into a wireless access point with the help of hostapd and dnsmasq.

OLIP itself runs into a container and seat on top of [Balena Engine](https://www.balena.io/open/) which is a lighter version of Docker especially developped for IOT/embedded context. 

OLIP take care of all the magic and communicates directly with the Docker engine to pop up container based WebApps.

Currently the configured device works only in stand alone wireless access point and can not be integrated in a local network without technical setup on local DHCP and DNS server.

![](olip-layers.png)
