---
title: Installation
comments: false
weight: 3
pre: "<b>3. </b>"
---

There is two ways of installing and testing OLIP
* You can install OLIP with the command line below, it will use an Ansible playbook to configure and install OLIP on a device. This is usually what we use for production.
* You can use a [Docker compose set of files](https://gitlab.com/bibliosansfrontieres/olip/olip-development#run-hack-olip-locally) to run & test OLIP locally on your computer. This is the fatest way to try out OLIP

{{% notice info %}}
OLIP has been tested on amd64 servers, CMAL 100 i386 content access point and Raspberry Pi 2 and older. It runs under Debian Buster and Ubuntu 16.04
{{% /notice %}}

Run the following command to install OLIP:

```shell
curl -sfL https://gitlab.com/bibliosansfrontieres/olip/olip-deploy/raw/master/go.sh | sudo bash -s -- --name mybox --url mybox.lan --descriptor http://olip.ideascube.org/amd64
```

{{% notice warning %}}
On **Raspeberry Pi**, make sure this one is equipped with an Internal / External wifi antenna. Use `ip a l` and look for `wlan0` which is usually the wifi interface. Please refer to the [wifi adapters list](https://elinux.org/RPi_USB_Wi-Fi_Adapters) for further information.
{{% /notice %}}

{{% notice info %}}
On Raspberry Pi, you can follow this [nice tutorial](https://projects.raspberrypi.org/en/projects/raspberry-pi-setting-up) to setup your Pi.
To enable SSH remote access, create a file called `ssh` in `/boot/` partition.
{{% /notice %}}

{{% notice info %}}
On Raspberry Pi, you can store the data on an external storage instead of the SD card.
Format an USB drive with an `ext4` partition and insert it in the device. The drive will be automatically detected and mounted. WebApps and their content will be stored on it.
{{% /notice %}}

---
#### Arguments :

- `--name` : your server name
- `--url`: URL used to access your device. Depending if you are online or offline you will have to set DNS configuration for this domain name
- `--descriptor` : URL to the folder where the `descriptor.json` file and apps thumbnails are stored. Please note that OLIP does not handle TLS/SSL, so, no **HTTPS** URL.
  For testing purpose, you can store the descriptor folder locally and use it this way : `file:///data/descriptor/arm32v7`
- `--extra-vars` : Allows you to set extra variables, such as OLIP Dashboard & API Docker image version or activate statistics on the device
  - `olip_dashboard_version=lastest` use the `latest` version of OLIP Dashboard ([available tags](https://hub.docker.com/r/offlineinternet/olip-dashboard/tags?page=1&ordering=last_updated)).
  - `olip_api_version=lastest` use the `latest` version of OLIP API ([available tag](https://hub.docker.com/r/offlineinternet/olip-api/tags?page=1&ordering=last_updated)).
  - `proxy_enable=true` enable a reverse proxy to collect logs and build [statistics](https://gitlab.com/bibliosansfrontieres/olip/olip-user-documentation/-/wikis/en/Statistics) with [GoAccess software](https://gitlab.com/bibliosansfrontieres/olip/goaccess#goaccess)

## Domain setup

You can use any local domain (eg: `.local`, `.lan`, etc.), the wifi hotspot and the DNS server will take care to serve IP address and resolve DNS requests from clients under this domain name.

### Online server

If your server works online, you can use a domain such as `example.com`.

You then need to add a few DNS records:

* a `A` field for `example.com`
* A wildcard subdomain `CNAME`s pointing to the `A` record for the WebApps: `kiwix.example.com`, `mediacenter.example.com`, etc

Your DNS zone should look like this (BIND format):

```text
example.com   IN A      192.2.0.13
*             IN CNAME  example.com.
```

## Admin account

Once your server is running you can connect to the wifi hotspot and/or the online URL, in our example <http://mybox.lan>.

Default administrator account is :

- login : `admin`
- password : `admin`

## External access

If you want to route external connection to the device (in case you are behind a firewall), open the following firewall inbound ports :

- OLIP Dashboard : `80`
- OLIP API : `5002`
- OLIP WebApps will listen on range `10000-20000`, i.e. first WebApp installed will listen on port 10000.

## Check the logs

At this point, the `olip-api` container logs should be checked for eventual errors:

```shell
balena-engine logs -tf api
```

{{% notice info %}}
Refer to [How to debug a WebApp](../knowledge-base/debug-an-app/) for further information.
{{% /notice %}}
