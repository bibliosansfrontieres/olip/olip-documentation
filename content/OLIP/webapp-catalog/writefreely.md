---
title: WriteFreely
comments: false
pre: "<b>- </b>"
---

### Description

An open source platform for building a writing space on the web.

![Mediacenter](../writefreely_logo.png?width=20pc)

### OLIP integration

| Feature | Description |
| ------ | ----------- |
| Content | None. |
| Unified search | ? |
| Authentication | ? |
