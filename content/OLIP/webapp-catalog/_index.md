---
title: WebApp catalog
comments: false
weight: 4
chapter: true
pre: "<b>4. </b>"
---

# WebApps Catalog

## Current Apps catalog

The catalog is currently featuring a few WebApps:

- [Mediacenter](./mediacenter): a mediacenter
- [Karibu](./karibu): apprentissage du Kirundi
- [Kiwix](./kiwix): a offline browser for websites
- [Kolibri](./kolibri): LMS
- [Moodle](./moodle): LMS
- [Nextcloud](./nextcloud): local Cloud for files, contacts, agenda
- [Surfer](./surfer): a simple fileserver which allows for uploads
- [Writefreely](./writefreely): a blogging platform

## Special Apps

These images have a special purpose:

- [MariaDB](https://gitlab.com/bibliosansfrontieres/olip/apps/mariadb): a companion image for Apps which requires a MySQL database (such as Moodle)
- [olip-base](../packaging-tutorial): a base image for all Apps, providing a common set of tools and mechanisms
- [php-5.3-apache](https://gitlab.com/bibliosansfrontieres/olip/apps/php5.3-apache): PHP 5.3 with Apache - required for a specific project
- [node-v12.9](https://gitlab.com/bibliosansfrontieres/olip/apps/node-v12.9): `olip-base` + NodeJS

## Contributing Apps

Contributions are welcome! See [Packaging a WebApp tutorial](../packaging-tutorial).
