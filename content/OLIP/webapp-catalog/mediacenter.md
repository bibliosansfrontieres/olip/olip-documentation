---
title: Mediacenter
comments: false
pre: "<b>- </b>"
---

### Description

A static website where the user is able to browse content
(images, songs, videos, etc.) agregated from several packages.

![Mediacenter](../mediacenter_screenshot.png?width=20pc)

### OLIP integration

| Feature | Description |
| ------ | ----------- |
| Content | The content (ZIP archives) can be managed by OLIP. |
| Unified search | OLIP can search mediacenter content. |
| Authentication | No restricted access, everyone can browse the content. |
