---
title: Moodle
comments: false
pre: "<b>- </b>"
---

### Description

Moodle is a LMS.

{{% notice info %}}
This App was created for a specific project. However, the project's contents were not intended for public distribution and have been removed from the catalog.\\
As a result, this Moodle integration comes with a few configurations and plugins, but no content is defined in the catalog.\\
Nevertheless, creating or importing courses is possible.
{{% /notice %}}

![Moodle](../moodle_logo.png?width=20pc)

### OLIP integration

| Feature | Description |
| ------ | ----------- |
| Content | The content **could** be managed by OLIP. |
| Unified search | ? |
| Authentication | No restricted access, everyone can browse the content. Registration is allowed to track progress. |
