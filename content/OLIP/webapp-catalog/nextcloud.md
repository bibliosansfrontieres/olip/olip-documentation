---
title: Nextcloud
comments: false
pre: "<b>- </b>"
---

### Description

Nextcloud is a suite of client-server software for creating and using file hosting services. Nextcloud provides functionality similar to Dropbox, Office 365 or Google Drive. Translations in 60 languages exist for web interface and client applications.

![Nextcloud](../nextcloud_logo.png?width=20pc)

### OLIP integration

| Feature | Description |
| ------ | ----------- |
| Content | None. |
| Unified search | ? |
| Authentication | ? |
