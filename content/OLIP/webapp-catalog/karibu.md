---
title: Karibu
comments: false
pre: "<b>- </b>"
---

### Description

Karibu is a LMS to learn [Kirundi](https://en.wikipedia.org/wiki/Kirundi).

Karibu is based off Moodle.

![Karibu](../karibu_logo.png?width=20pc)

### OLIP integration

| Feature | Description |
| ------ | ----------- |
| Content | The content is integrated into the Image. |
| Unified search | ? |
| Authentication | No restricted access, everyone can browse the content. Registration is allowed to track progress. |
