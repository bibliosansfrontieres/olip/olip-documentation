---
title: Surfer
comments: false
pre: "<b>- </b>"
---

### Description

Surfer is a simple static file server:

* Logged in users can upload files or folders from their device
* Visitors can browse files
* Any `index.html` file in a directory will be served up automatically

![Surfer](../surfer_screenshot.png?width=20pc)

### OLIP integration

| Feature | Description |
| ------ | ----------- |
| Content | The web interface allows to browse, upload and download the content |
| Unified search | No search feature at all |
| Authentication | Everyone can browse the content, **local** login required for uploads |
