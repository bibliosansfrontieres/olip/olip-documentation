---
title: Kolibri
comments: false
pre: "<b>- </b>"
---

### Description

Kolibri is an open-source educational platform specially designed to provide offline access to a wide range of quality, openly licensed educational contents in low-resource contexts like rural schools, refugee camps, orphanages, and also in non-formal school programs.

Kolibri can serve high quality education content from several publicly available content channels, collections of educational resources (exercises, videos, audio or document files) and associated metadata, prepared and organized for their use in Kolibri. It works in conjunction with Kolibri Studio, the curriculum tool used to organize content and build custom content channels, aligned to the local curricula, or according to specific learning needs.

![Kolibri](../kolibri_logo.png?width=20pc)

### OLIP integration

| Feature | Description |
| ------ | ----------- |
| Content | The contents (channels) are managed by Kolibri itself, even tough content could be managed by OLIP. |
| Unified search | OLIP can search Kolibri content. |
| Authentication | Kolibri is able to authenticate and grant access to OLIP users. |
