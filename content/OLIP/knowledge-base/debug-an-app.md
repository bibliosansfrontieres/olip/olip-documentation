---
title: How to debug a WebApp ?
comments: false
weight: 2
pre: "<b>- </b>"
---

Sometimes a WebApp might crash, considering they are container based
they might be tricky to debug.

You will find below some useful tips to understand what is happening under the hood!

OLIP is using `balena-engine` as a Docker Daemon.
The binary command will be `balena-engine` instead of `docker`.

## List installed images

OLIP only show the downloaded Apps images.

The Docker daemon may list more:

```shell
# balena-engine images
REPOSITORY                         TAG       IMAGE ID        CREATED        SIZE
offlineinternet/dweb-mirror        latest    44c3eab0f0c6    4 days ago     306MB
offlineinternet/hugo-mediacenter   latest    1d41ce151363    6 days ago     206MB
offlineinternet/olip-dashboard     develop   e63bbdbc9f62    11 days ago    117MB
offlineinternet/dweb-mirror        <none>    4e6b9ebd9c47    13 days ago    607MB
```

## List running containers

### In OLIP

Images are listed in OLIP when the application has been installed.

Sometimes they might go back and forth between
downloaded applications and installed applications.

This is usually the sign of an issue within the container.
OLIP is not able to start the application as the container is crashing for some reason.

### Docker daemon

```shell
~# balena-engine container ls
CONTAINER ID    IMAGE                                     COMMAND                  CREATED          STATUS                         PORTS                     NAMES
b17ca6888f40    offlineinternet/dweb-mirror:latest        "docker-entrypoint.s…"   30 minutes ago   Restarting (1) 3 seconds ago                             dweb-mirror.app.dweb-mirror
c937146e944a    offlineinternet/hugo-mediacenter:latest   "/docker/entrypoint.…"   6 days ago       Up 6 days                      0.0.0.0:10001->3000/tcp   mediacenter.app.mediacenter
04d93c161287    offlineinternet/kiwix:latest              "/docker/entrypoint.…"   11 days ago      Up 11 days                     0.0.0.0:10000->8080/tcp   kiwix.app.kiwix
7e3ac3c750f4    offlineinternet/olip-dashboard:develop    "sh /startup.sh ngin…"   11 days ago      Up 11 days                     0.0.0.0:80->80/tcp        olip-dashboard
e0753c6ed902    offlineinternet/olip-api:develop          "/opt/api/docker/ent…"   13 days ago      Up 11 days                     0.0.0.0:5002->5002/tcp    olip-api
```

`PORTS` and `NAMES` are defined by OLIP,
partly based on informations from the `descriptor.json` file.

Notice the `dweb-mirror` App Status?
The `Restarting` state means the container fails starting.
This deserves some investigation.

## Understand what is happening in the container

So our container is crashing.

First of all, we want to get some logs.

If the Docker image developer has [redirected the application logs](https://docs.docker.com/config/containers/logging/)
to `stdout` and `stderr` you should see some informations:

```shell
balena-engine logs -f dweb-mirror.app.dweb-mirror
```

Possible reasons:

- a broken `entrypoint.sh` script
- a wrong `CMD` Docker command
- an issue in the web application

## Get into a container

### A running container

```shell
balena-engine exec -ti dweb-mirror.app.dweb-mirror bash
```

### A stopped container

We need to start manually the container in order to understand what is going on.
To achieve this, we can simply run the container and get into it.

If your `ENTRYPOINT` or `CMD` fails, the container won't start and you won't be
able to enter it. You then have to override it:

#### Override the entrypoint

```shell
balena-engine run -ti --rm  --entrypoint="" offlineinternet/hugo-mediacenter bash
```

- `--entrypoint=""` : in this case we remove the entrypoint
- `--entrypoint="my_new_entrypoint.sh"`: You can also write a new one.

Note that this file will have to be in the container (see [Docker mount](https://docs.docker.com/storage/volumes/)).

#### Override the CMD command

```shell
balena-engine run -ti --rm offlineinternet/hugo-mediacenter my_app
```

{{% notice info %}}
The `entypoint.sh` script will still be executed!
{{% /notice %}}

#### Override both

```shell
balena-engine run -ti --rm  --entrypoint="" offlineinternet/hugo-mediacenter bash
```

Here you get into a brand new container without entrypoint enabled
and with a command line enabled.

Please open an issue in the Webapp's GitLab Project.
