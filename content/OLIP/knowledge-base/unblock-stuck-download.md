---
title: How to unblock a stuck Download ?
comments: false
weight: 1
pre: "<b>- </b>"
---

Currently there is nothing that can be done from the dashboard. This happen usually when OLIP can not reach the remote image given in the image parameter of the `descriptor.json` file. It can also happen when the container can not be start up.

## Logs

The only way to understand what is happening is to check out logs from OLIP API - from the device's shell:

```shell
balena-engine logs -f olip-api
```

This command will show you the state machine who is doing all the jobs behind the scene. At some point you should see some informations about the WebApp installation with hopefully some error to investigate on.

## Reset

If you reach the point where there is nothing to be done, remove the database from `/data/app.db`,
eventually the WebApp (`webappname.app`) folder created in `/data/`.

Restart balena-engine:

```shell
systemctl restart balena-engine
```

You will then start again from a fresh install.
