---
title: The OLIP Base Image
weight: 1
subtitle: 
pre: "<b>- </b>"
comments: false
---

All WebApps are based uppon `registry.gitlab.com/bibliosansfrontieres/olip/apps/olip-base/olip-base:latest`.

This allows to share Docker Layers across all Webapp,
thus reducing the required storage.

It also provides a few tools, features and mechanisms:

- tools (i.e. git, cron, python, sqlite3)
- an init system to run the main service and cronjobs
- some [Content Management Scripts](../content-management-scripts/)

### Init system

The init is based off `supervisord`.

Services files are to be dropped at `/etc/supervisor/conf.d/`.
The services files must have the `.conf` extension.

This is a typical `app.conf` which runs the main service
(here, `kiwix-serve`), and the content management script:

```ini
[supervisord]
nodaemon=true

[program:kiwix]
directory=/usr/local/bin
command=/usr/local/bin/kiwix-serve --library /data/library.xml --port 8080
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0

[program:olip-queue-management]
command=/bin/bash -c "sleep 1m && /usr/local/bin/olip-queue-management.py"
directory=/usr/local/bin/
autostart=true
autorestart=true
stdout_logfile=/dev/stdout
stdout_logfile_maxbytes=0
stderr_logfile=/dev/stderr
stderr_logfile_maxbytes=0
```
