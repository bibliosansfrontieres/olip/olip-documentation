---
title: The Docker Image
subtitle: The OLIP WebAPP Docker Image specification
comments: false
chapter: true
pre: "<b>> </b>"
---

# In a nutshell

## Base image

The image is created `FROM olip-base`

Learn more in the [Base Image](the-base-image/) section.


## Content Management scripts

It must provide some scripts for content management:

* `install_content.sh`
* `uninstall_content.sh`
* `upgrade_contents.sh`

Learn more in the [Content Management Scripts](content-management-scripts/) section.


## Data persistence

All data must remain in `/data/` in the image.

This directory will be mounted in `/data/<webappname>.app/content/` on the host.
