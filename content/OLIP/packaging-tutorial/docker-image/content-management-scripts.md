---
title: The Content Management Scripts
weight: 2
subtitle: 
pre: "<b>- </b>"
comments: false
--- 

The `olip-base` images comes with a default set of content management scripts.

These three script receive some arguments:

- `$1`: `content_id` as defined in the catalog
- `$2`: `download_path` as defined in the catalog

By default, the scripts only `echo` the `content_id`,
which is a no-op.

Since every App is different, you probably have to write your own
scripts for installing, updating and removing content.

## Examples

### install_content.sh

This script is in charge to add content.

A very simple example:

```sh
/opt/venv/bin/kolibri manage importchannel network $1
```

### uninstall_content.sh

This script removes content.

Here's a more evolved example:

```sh
#!/bin/bash

say() {
    >&2 echo "$( date '+%F %T') uninstall_content.sh: $*"
}

id=$( /usr/local/bin/kiwix-manage /data/library.xml show | grep -F "$2" -B1 | awk ' /^id/ { print $2 } ' )

[ -z "$id" ] && {
    say "WARNING: ZIM $2 not found in the library file, nothing to do."
    exit 0
}

say "uninstalling ${2}..."
/usr/local/bin/kiwix-manage /data/library.xml remove "$id"
supervisorctl restart kiwix
```

### upgrade_content.sh

This script updates content.

## olip-queue-management.py

The `olip-api` container stores contents mangement orders into
a SQLite database queue file:

- from `olip-api`: `/data/<webappname>.app/content/queue`
- from the App container: `/data/content/queue`

In the App container, the `olip-queue-management.py` script is in
charge of looking at the queue, and runs the appropriate script.

This script does not have to be modified.
