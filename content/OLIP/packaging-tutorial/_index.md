---
title: Packaging a WebApp
pre: "<b>5. </b>"
comments: false
chapter: true
weight: 5
---

# WebApp Packaging

This tutorial outlines how to package a web application for OLIP.

Creating an application for OLIP can be summarized as follows:

1. Create a [Dockerfile](http://docs.docker.com/engine/reference/builder/) for your application.
2. Build that app using `docker build` and push the image to any public Docker registry using `docker push`. To help out with the automatic build and push workflow we use the Gitlab CI (ex: with Kolibri: [gitlab-ci.yml](https://gitlab.com/bibliosansfrontieres/olip/apps/kolibri/blob/master/.gitlab-ci.yml), [Dockerfile](https://gitlab.com/bibliosansfrontieres/olip/apps/kolibri/blob/master/Dockerfile))
3. Create or add an entry in [OLIP catalog](../packaging-tutorial/register-app-catalog/) and host it locally or remotelly
4. Install you WebApp








