---
title: Add files to your repository
subtitle: 
weight : 3
pre: "<b>- </b>"
comments: false
---

## Repository layout

### Minimal layout

The bare minimum for an OLIP App repository is:

- the `Dockerfile` to define your image
- the `.gitlab-ci.yml` file to build it

### Typical layout

A complete App would look like this:

```
.
├── scripts                   # content management scripts specific to the App
│   ├── install_content.sh
│   ├── uninstall_content.sh
│   └── update_content.sh
├── supervisor                # services files for the App
│   └── app.conf
├── Dockerfile
├── entrypoint.sh
├── .gitlab-ci.yml
└── README.md
```

## Create a Dockerfile

```
# hadolint ignore=DL3007
FROM offlineinternet/olip-base:latest

RUN apt install nginx \
    echo '<h1>Hello world!</h1>' > /var/wwww/index.html

EXPOSE 80

CMD ["nginx", "-g", "daemon off"]
```

## Create a `.gitlab-ci.yml` file

```yaml
---
include:
  - file:
    project: 'bibliosansfrontieres/olip/cicd'
    file:
      - '/docker.recipe.yml'
      - '/deploy.definitions.yml'
      - '/deploy.pipeline.yml'
      - '/linters.definitions.yml'
      - '/linters.pipeline.yml'
    ref: v1.0.0

stages:
  - lint
  - build
  - deploy
```

## Commit your changes

```shell
git add Dockerfile .gitlab-ci.yml
git commit -a -m "Add Dockerfile and gitlab-ci"
git push origin main
```

This will trigger the CI, and the build/publish process will start.

## Check the result

Make sure everything worked as expected:

- the GitLab pipeline is all green
- the image is published in both GitLab and Docker Registries
