---
title: Setup the catalog
subtitle: 
comments: false
weight: 5
pre: "<b>> </b>"
---

We use a [descriptor.json](http://olip.ideascube.org/amd64/descriptor.json) file as a catalog to declare WebApps and their related content. The operation of adding a WebApp to the catalog consists in adding a new entry to this JSON file. 

Keep in mind that each OLIP device can be linked to **ONLY ONE** `descriptor.json` file, however you can have several OLIP devices linked to a single `descriptor.json`.

At Libraries Without Borders we choose to link all our devices to one `descriptor.json` file: <http://olip.ideascube.org/amd64/descriptor.json>

Linking an OLIP instance to a `descriptor.json` file is done during OLIP deployment and configuration, please refer to the [installation chapter](../../installation) for further information.

With that being said, we are going to add a new entry in our `descriptor.json`file, whether you are going to host it locally or remotely the method is the same.

{{% notice tip %}}
Please note that `applications` and `contents` have a version number each. It will start with 0 and increase with each shipped release. Each version increment will let the OLIP administrator update either WebApp and/or content.
{{% /notice %}}
