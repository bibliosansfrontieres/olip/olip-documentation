---
title: Authentication
weight: 1
subtitle: 
comments: false
pre: "<b>- </b>"
--- 

The platform integrates an OpenId Connect (OIDC) autorization server in order for the applications to make users authenticate themselves against the user-base managed by the platform.

It allows application developers to avoid re-creating each time a user management feature. Besides, users of the platform may navigate amongst applications without having to re-authenticate. Of course, application may choose to provide their own user management mechanism, but users will then need to re-authenticate via these specific credentials.

OIDC being an additional layer on top of the OAuth 2 protocol, the platform is able to provide access delegation to applications, i.e. granting access tokens to applicatons, so that they can use the plaform API in place of the final user.

A minimal understanding of OAuth and OIDC is therefore required. This document will focus on what the platform provides. For more information, please refer to the OAuth 2 and OIDC specifications.

## Example integration

Exemple for Kolibri WebApp :

```json
		"authentication": {
			"grant_types": ["authorization_code", "implicit"],
			"token_endpoint_auth_method": "client_secret_basic",
			"response_types": ["id_token", "code id_token"]
		},
```

The authentication field is a nested object allowing to setup the OIDC integration if used by the application.

| field | description |
| --- | --- |
| grant_types | Type of OIDC flow |
| response_types | Types of response requested by the app |
| token_endpoint_auth_method | Authentication mechanism used by the app on the platform |

## Grant Types

An OIDC/OAuth authorization server may be used for several use cases and topology of applications.

This is materialized by different grant types which change the flow of the authentication. Here is a desription of the grant types the platform currently supports:

- autorization_code: this grant type is used when there is a backend component on the client application (i.e. a standard Web MVC app or a single page app with an API). The platform's authorization server emits an authorization code, that may be used by the application's backend to retrieve an access_token (access delegation) and/or an id token (authentication). No access_token is sent to the user agent (browser), increasing the security of the solution.
- implicit: in this case, the autorization server directly returns the access token/id_token to the user agent. This is especially usefull for pure browser applications (i.e. with no backend) where redeeming an authorization token would bring no additionaly security value.

The autorization server checks the allowed grant types depending on what is specified in the application's descriptor in the authentication section. If your application only requires the autorization_code grant type (if you have a backend that is able to redeem an authorization code), you should then remove the implicit grant type from the list in the application descriptor.

There are other well-known grant types not supported (or not tested) yet:

- password grant: Allows application to directly gather the user's credentials (username and password) and redeem a token in exchange. Must only be used on trusted applications, therefore not useful for our context.
- client_credentials: allows application to authenticate themselves, i.e. without user authentication. May be usefull if application need to perform some background job without user interaction. Not impleted yet however.
- refresh_token: Access token may be set to expire. The refresh token is used to refresh the access token when it is expired.

## Response types

The response types defines what is the result of an authorization request. There are several response types supported by the platform:

- code: The code response type will request an authorization code from the the authorization server. This is the typical response type for the authorization_code grant type.
- id_token: The id token is a JWT token that contains a proof of the user's identity.
- token: The token response type requests directly an access token from the authorization server. This is the typical response type used by the implicit grant type.

Client may request a combination of these response types. The authorization server will then select a flow accordingly: If the code response type is requested, the authorization server will switch to the authorization code grant type. If the token response type is selected, we are following the implicit flow. If a combination of response types is selected, we are in an "hybrid" case.

The response types that an application may request must be specified in the response_types entry of the authentication object in the descriptor. The current list of possibilites is:

- code
- code id_token
- id_token
- token id_token

## Endpoint auth method

In order to secure the application that are allowed to request something from the authorization server, each application must authenticate against the authorization server (at least for the authorization_code grant type).

The credential used by appliciations are provided by each container via environment variable. The following variables are available:

- CLIENT_ID: Id of the client. On this platformn this is the application bundle
- CLIENT_SECRET: Password randomly generated by the platform. It is re- generated at each installation of the application.
- OIDC_URL: Url of the authorization provider. The authorization server supports dynamic discory on the url $OIDC_URL/.well-known/oidc-configuration.

Depending on the application, two authentication methods are available:

- client_secret_post: credentials are sent via a POST request to the authorization server
- client_secret_basic: credentials are sent via the authorization header

The expected authentication method must be specified in the descriptor in the token_endpoint_auth_method.

