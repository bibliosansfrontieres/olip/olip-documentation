---
title: Search
weight: 2
subtitle: 
pre: "<b>- </b>"
comments: false
--- 

Exemple for Kolibri WebApp :

```json
		"search": {
			"opensearch_url": "/opensearch/",
			"container": "kolibri"
		}
```

The search field is a nested object describing the opensearch interface that may be exposed by application in order to integrate with the global search functionality.

| field                      | description                                                  |
| -------------------------- | ------------------------------------------------------------ |
| container                  | Name of the container. Must match the description of a container in the app |
| opensearch_url             | URL, relative to the container root, to the opensearch descriptor |
| token_endpoint_auth_method | Authentication mechanism used by the app on the platform     |

## Implementing the search API

The platform provides a global search features, where the user can search among all applications providing the required interface.

The platform itself implements an Opensearch interface. The request is delegated to other applications, and a single Atom feed is returned.

The path to the opensearch descriptor is `/opensearch`. It contains the following URL template for perform queries:  `/opensearch/search?q={searchTerms}`

The received term is forwarded to every application exposing an Opensearch interface by itself. The URL used to request a search from an application is built from the exposed port from the container. For instance, if the container is exposed on port 10000, the URL triggered by the platform will be `http://<platform_ip>:10000/<opensearch_url>`, the opensearch_url beeing the parameter specified in the application's descriptor under the search section.

In order to provide absolute URLs, containers deployed via the platform have access to the `APPLICATION_ROOT` environment variable, that specifies the root URL of each container.
