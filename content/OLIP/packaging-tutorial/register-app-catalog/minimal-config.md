---
title: Minimal configuration
weight: 2
subtitle: 
pre: "<b>- </b>"
comments: false
--- 

Here is the bare minimal setting to declare for a new WebApp : 

```json
{
	"applications": [{
		"version": "0.0.1",
		"picture": "#dweb-mirror.png",
		"name": "Dweb-mirror",
		"containers": [{
			"image": "offlineinternet/dweb-mirror:latest",
			"expose": 4244,
			"name": "dweb-mirror"
		}],
		"bundle": "dweb-mirror.app",
		"description": "The Offline Internet Archive project"
	}]
}
```

## Add some content

OLIP provides a way to manage optional content directly via the administration interface.

To do so, you have to add few more lines to declare content in the content array. Check out this [descriptor.json](http://olip.ideascube.org/amd64/descriptor.json) file as an example.

```json
{
	"applications": [{
		"version": "0.0.1",
		"picture": "#dweb-mirror.png",
		"name": "Dweb-mirror",
		"containers": [{
			"image": "offlineinternet/dweb-mirror:latest",
			"expose": 4244,
			"name": "dweb-mirror"
		}],
		"bundle": "dweb-mirror.app",
		"description": "The Offline Internet Archive project",
		"contents": [{
			"version": "0.0.1",
			"size": "1115955214",
			"endpoint": {
				"url": "/geography/",
				"container": "dweb-mirror",
				"name": "This content is about geography"
			},
			"name": "Geography",
			"download_path": "http://file.server.com/my_archive.zip#unzip",
			"destination_path": "geography/",
			"content_id": "geography",
			"description": "This content is about geography",
			"language": "en",
			"subject": "my_category_name"
		}]
	}]
}
```

## Pass variables to the container 

In some case you might want to know on what system your WebApp is running. 

For such case, you can add some informations to the entry:

```json
		"containers": [{
				"image": "offlineinternet/dweb-mirror:latest",
				"expose": 4244,
				"name": "dweb-mirror",
				"configuration": {
					"platform=olip": {
						"description": "Platform running on"
					},
					"arch=amd64": {
						"description": "Architecture"
					}
				}
			}],
```

that you will find back in the container:

```text
$  docker inspect 02c | grep -A5 Env
            "Env": [
                "platform=olip",
                "CLIENT_ID=dweb-mirror.app",
                "arch=amd64",
```

## Descriptor Fields

Here is a description of each fields of the descriptor:

| field       | description                                                  |
| ----------- | ------------------------------------------------------------ |
| bundle      | System name of the app. Must be unique over all available applications. |
| name        | This is the display name of the app, as displayed on the home screen. |
| description | Shown on the apps download screen. |
| version     | Version of the app. [Semantic Versioning](https://semver.org/) is welcome! |
| picture     | base64 encoded PNG image, shown on the home and catalog screen. |

### Container

The **container** field is a nested structure that contains the following fields:

| field  | description                                   |
| ------ | --------------------------------------------- |
| image  | Name of the image on Registries               |
| name   | Part of the name of the container (see below) |
| expose | Exposed port on the host                      |

### Contents

The **contents** array allows proposing additional content that may be downloaded from the administration interface.
Each entry in this array match the following structure:

| field            | description                                                  |
| ---------------- | ------------------------------------------------------------ |
| content_id       | Id of the content. Must be unique for one application.       |
| name             | Name of the content. Shown in the admin interface.           |
| version          | Version of the content. [Semantic Versioning](https://semver.org/) is ok, but a simple [Unix timestamp](https://en.wikipedia.org/wiki/Unix_time) does the job. |
| download_path    | Path (URL, IPFS pin, ...) to retrieve the content from.      |
| destination_path | Path of the symlink, relative to `/data/content/`            |
| description      | Longer description of the content                            |

### Endpoint

An **endpoint** can be specified in content, so that it is accessible from categories including that content. The following attributes can be specified in the endpoint properties:

| field     | description                                                  |
| --------- | ------------------------------------------------------------ |
| container | Name of the container on which this content can be accessed. Must match the name of a declared container. |
| name      | Name of the endpoint. Will be displayed in the interface.     |
| url       | URL where the content can be accessed, starting from the root of the context. |
