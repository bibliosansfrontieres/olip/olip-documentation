---
title: Content and data persistence
weight: 3
subtitle: 
pre: "<b>- </b>"
comments: false
--- 

## Access your content from your container

In the JSON just above, you might have seen two lines:

### download_path

```json
"download_path": "http://file.server.com/my_archive.zip#unzip",
```

The `download_path` can be a file or an archive.

Please note that you have to add `#unzip` to tell OLIP to unzip the archive.

{{% notice info %}}
For now, ZIP is the only compression algorithm used by OLIP.
{{% /notice %}}

### destination_path

```json
"destination_path": "geography/"
```

Once unzipped, content archive will be moved into the `destination_path` folder.

{{% notice warning %}}
Please note the trailing slash (important !).
{{% /notice %}}

You can access the content either :

* From host system : `/data/dweb-mirror.app/content/geography/`
* From the Docker container : `/data/content/geography/`

Content managed by OLIP will be always stored in `/data/content/` inside the container.

## Data persistence

You might wonder how your WebApp can save some data across container reboot.

OLIP will mounth the host's `/data/dweb-mirror.app/` folder to the container's `/data/` folder.

Hence, configure your WebApp to store the files under `/data/` into you container.
