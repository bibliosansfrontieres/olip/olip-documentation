---
title: Where to host the catalog ?
weight: 1
subtitle: 
pre: "<b>- </b>"
comments: false
---

The catalog can be hosted either locally on your server or remotely on a HTTP server. 

* **Locally** : Great for testing purpose, the catalog will be access only by **your** server but you'll have to duplicate it if you want it on several device. 
* **Remotely** : Good for production, setup a basic HTTP server and serve the content. All the devices pointing this URL will synchronize the catalog locally

The structure is the following : 

```
amd64/
├── bsfcampus.png
├── descriptor.json
├── generic.png
├── kiwix.png
├── kolibri.png
└── nextcloud.png
```

The `descriptor.json` file must remain in a folder, at the same level than the applications thumbnails.

