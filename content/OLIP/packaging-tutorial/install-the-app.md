---
title: Install the WebApp
subtitle: 
weight : 6
pre: "<b>- </b>"
comments: false
---

At this point you have:

* A GitLab repository that hosts your WebApp source code
* A continuous integration and delevery system that automatically builds the Docker image according to your Dockerfile
* A catalog that holds the necessary informations for your WebApp to be recognized by OLIP platform. Your catalog might be hosted [locally or remotelly](../register-app-catalog/where-to-host-catalog).

We now have to install the WebApp and its content on OLIP. 

1. The catalog is updated automatically every 5 minutes, but you can force the update with <http://olip.lan:5002/applications/?visible=true&repository_update=true>
2. Head over the OLIP platform (your URL might be something like <http://olip.lan>) and connect as Administrator (defaults credentials are `admin`/`admin`)
3. As administrator, click on the [catalog link](../../end-user-doc/07.catalog-under-construction/default/) and fellow instructions to download your WebApp
4. Once your WebApp is downloaded, click on the [Applications link](../../end-user-doc/08.application/default/) and install your WebApp with your content if any is available.
5. Click on [home link](../../end-user-doc/01.homepage/default/) to see your WebApp in action
