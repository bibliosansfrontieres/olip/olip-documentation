---
title: Setup the Docker repository
weight: 2
subtitle: 
pre: "<b>- </b>"
comments: false
---

## Docker Hub Registry

Images are pushed to the [Docker Hub Registry](https://hub.docker.com/u/offlineinternet)
for convenience as this is the default registry.

### Creation

Create a new repository in the
[**offlineinternet** group](https://hub.docker.com/u/offlineinternet),
such as `offlineinternet/my-app`:

- set access to public
- the repository name must match the GitLab Project's slug

### Configure permissions

Add the `contrib` group with `Read & Write` Permission,
so the CICD bot is allowed to publish images.

## GitLab Registry

The CI also pushes the images to the Gitab project's Docker Registry.

This repository is automatically created at the first push from the CI.

The catalog points images to this repository,
in order to not hit the Docker Hub Registry quotas.
