---
title: Create the GitLab repository
weight: 1
subtitle: 
pre: "<b>- </b>"
comments: false
---

Create a **New project** in the [`/olip/apps/` subgroup](https://gitlab.com/bibliosansfrontieres/olip/apps/):

- the project's slug will be the App's identifier
- set the repo access to public

This project will inherite from the OLIP group:

- GitLab Runners (maintened by Library Without Borders)
- Group Variables (Docker Hub credentials)
- Slack notifications (commits, issues, pipelines, ...)
