---
title: Homepage
subtitle: 
weight: 1
comments: false
---
OLIP is an open source software developed by the offline Internet consortium to turn any device into an offline server.

It makes it easy to install WebApps and manage their content on your server.

OLIP provides a search engine for global search within each WebApp, and hosts an Authentication server based on OAuth.

![](image-20200114111001085.png)
