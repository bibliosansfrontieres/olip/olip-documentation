# Contributing

This project uses the [Hugo](https://gohugo.io/) static site generator.

## Cloning

This project uses [`matcornic/hugo-theme-learn`](https://github.com/matcornic/hugo-theme-learn)
as a submodule:

```shell
git submodule init
git submodule update
```

Thse operations can be performed at clone stage directly:

```shell
git clone --recurse-submodules git@gitlab.com:bibliosansfrontieres/olip/olip-documentation.git
```

## Using Hugo

### Prebuilt binary

Hugo is written in Go, and as usual it provides a
[pre-built binary](https://gohugo.io/getting-started/installing/#binary-cross-platform).

Make sure you use the same version
([v0.58.3](https://github.com/gohugoio/hugo/releases/tag/v0.58.3) as of writing).

Provided the `hugo` binary is in your `$PATH` you can use it to serve the website.
Thanks to the [LiveReload](https://gohugo.io/getting-started/usage/#livereload) feature,
you don't have to build the website to see your changes applied.

### Docker image

A convenient shell alias using a
[Docker image](https://hub.docker.com/layers/klakegg/hugo/0.58.3-ubuntu/images/sha256-2d28b3cd073ecd5e2c8edb826455b93acb6782c5494390f9d99cc9417d5efc29?context=explore):

```sh
hugo-olip ()
{
  docker run -it --rm \
    --name hugo-olip \
    -v "${PWD}:/src" \
    klakegg/hugo:0.58.3-ubuntu $@
}
```

## Hack it

Create your own branch, hack it, commit.

## Merge request

Once you are satisfied by your changes, push your branch and create a Merge Request.
It will be reviewed and (hopefuly) merged soon.
