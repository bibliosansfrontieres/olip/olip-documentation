# OLIP Developer Documentation

This is the repository for the OLIP Developer Documentation source.
See <http://bibliosansfrontieres.gitlab.io/olip/olip-documentation/>

Contributions are welcome! See the [Contributing guide](CONTRIBUTING.md).

The OLIP User Documentation is available at <https://gitlab.com/bibliosansfrontieres/olip/olip-user-documentation>.
